#include "Platform/Platform.hpp"

using namespace std;
using namespace sf;

int main()
{
	util::Platform platform;

#if defined(_DEBUG)
	cout << "Deployed!" << endl;
#endif

	RenderWindow window;
	// in Windows at least, this must be called before creating the window
	float screenScalingFactor = platform.getScreenScalingFactor(window.getSystemHandle());
	// Use the screenScalingFactor
	window.create(VideoMode(1000.0f * screenScalingFactor, 1000.0f * screenScalingFactor), "Genetic rocket!");
	platform.setIcon(window.getSystemHandle());

	CircleShape shape(100);
	shape.setFillColor(Color::Green);
	shape.setPosition(window.getSize().x / 2 - 100, window.getSize().y / 2 - 100);

	Event event;

	while (window.isOpen())
	{
		while (window.pollEvent(event))
		{
			if (event.type == Event::Closed)
				window.close();
		}

		window.clear();
		window.draw(shape);
		window.display();
	}

	return 0;
}
